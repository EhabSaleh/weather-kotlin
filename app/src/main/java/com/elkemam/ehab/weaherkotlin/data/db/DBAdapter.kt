package com.elkemam.ehab.weaherkotlin.data.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast


class DBAdapter(internal var c: Context)//  Toast.makeText(c,"called constructor and database was created",Toast.LENGTH_LONG).show();
    : SQLiteOpenHelper(c, DBName, null, version) {
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(
            "create table IF NOT EXISTS $tableName" +
                    "($id integer primary key autoincrement, " +
                    "$city varchar(50)," +
                    "$dt_txt varchar(50)," +
                    "$temp varchar(50)," +
                    "$humidity varchar(50)," +
                    "$pressure varchar(50)," +
                    "$speed varchar(50)," +
                    " $deg varchar(50));")
        Toast.makeText(c, "onCreate called", Toast.LENGTH_LONG).show()
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("drop table IF NOT EXISTS $tableName")
        onCreate(db)
        Toast.makeText(c, "onUpgrade called", Toast.LENGTH_LONG).show()
    }

    companion object {
        private val DBName = "Weather"
        private val version = 1
        private val tableName = "City"
        private val id = "ID"
        private val city = "city"
        private val dt_txt = "dt_txt"
        private val temp = "temp"
        private val humidity = "humidity"
        private val pressure = "pressure"
        private val speed = "speed"
        private val deg = "deg"
     }


}
