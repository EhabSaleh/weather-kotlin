package com.elkemam.ehab.weaherkotlin.ui.activity.city


import android.arch.lifecycle.ViewModel
//  import com.elkemam.ehab.weaherkotlin.data.entity.WeatherData
  import com.elkemam.ehab.weaherkotlin.data.entity.WeatherObject
 import com.elkemam.ehab.weaherkotlin.data.repository.WeatherRepo
 import io.reactivex.Observable

class CityViewModel(val productRepo: WeatherRepo) : ViewModel() {


    fun Getforecast(q: String ,id: String):Observable<WeatherObject>{
        return productRepo.forecast(q,id)
    }

}