package com.elkemam.ehab.weaherkotlin.ui.activity.main

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.elkemam.ehab.weaherkotlin.R
import com.elkemam.ehab.weaherkotlin.databinding.ActivityHomeBinding
import com.elkemam.ehab.weaherkotlin.ui.adapter.CityAdapter

import kotlinx.android.synthetic.main.activity_home.*

class Home : Fragment() {
    lateinit var binding: ActivityHomeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.activity_home, container, false)
        val source = arguments!!.getString("from")
        var array: ArrayList<String> = ArrayList()

        // ADD 5 Cites
        array.add("Egypt")
        array.add("India")
        array.add("canada")
        array.add("Spain")
        array.add("Brazil")
        binding.recycler.adapter = CityAdapter(array, context!!, source)
        binding.recycler.layoutManager = GridLayoutManager(context!!, 1)

        return binding.root
    }

}
