package com.elkemam.ehab.weaherkotlin.ui.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import com.elkemam.ehab.weaherkotlin.R


class SplashScreen : AppCompatActivity() {
    var thread: Thread? = null
    var constraintLayout: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        constraintLayout = findViewById<View>(R.id.liner) as LinearLayout

        // Click on Splash >> Stop Thread and start Main
        constraintLayout!!.setOnClickListener {
            thread!!.interrupt()
            val i = Intent(this, MainActivity::class.java)
            finish()
            startActivity(i)
        }


        // Sleep for 3 Second And Start Main Page
        thread = object : Thread() {
            override fun run() {

                try {
                    Thread.sleep(3000)
                    val i = Intent(baseContext, MainActivity::class.java)
                    finish()
                    startActivity(i)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

            }
        }
        thread!!.start()
    }
}
