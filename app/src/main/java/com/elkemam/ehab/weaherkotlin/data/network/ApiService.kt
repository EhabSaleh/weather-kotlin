package com.elkemam.ehab.weaherkotlin.data.network

  import com.elkemam.ehab.weaherkotlin.data.entity.*
  import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("forecast")
    fun Getforecast(@Query("q") city :String ,@Query("APPID") APPID :String  ): Observable<WeatherObject>

}