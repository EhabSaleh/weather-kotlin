package com.elkemam.ehab.weaherkotlin.data.entity

data class WeatherObject(
    val city: City,
    val cnt: Double,
    val cod: String,
    val list: List<X>,
    val message: Double
) {
    data class City(
        val coord: Coord,
        val country: String,
        val id: Double,
        val name: String,
        val population: Double
    ) {
        data class Coord(
            val lat: Double,
            val lon: Double
        )
    }

    data class X(
        val clouds: Clouds,
        val dt: Double,
        val dt_txt: String,
        val main: Main,
        val snow: Snow,
        val sys: Sys,
        val weather: List<Weather>,
        val wind: Wind
    ) {
        data class Weather(
            val description: String,
            val icon: String,
            val id: Double,
            val main: String
        )

        data class Main(
            val grnd_level: Double,
            val humidity: Double,
            val pressure: Double,
            val sea_level: Double,
            val temp: Double,
            val temp_kf: Double,
            val temp_max: Double,
            val temp_min: Double
        )

        data class Sys(
            val pod: String
        )

        data class Wind(
            val deg: Double,
            val speed: Double
        )

        data class Clouds(
            val all: Double
        )

        class Snow(
        )
    }
}

