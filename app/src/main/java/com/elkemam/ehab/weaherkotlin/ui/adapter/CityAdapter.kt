package com.elkemam.ehab.weaherkotlin.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.elkemam.ehab.weaherkotlin.R
import com.elkemam.ehab.weaherkotlin.ui.activity.city.CityWeather
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import com.elkemam.ehab.weaherkotlin.ui.fragment.offline.Offlinedata
import com.elkemam.ehab.weaherkotlin.util.checkInternetConnectivity


class CityAdapter(val categories: ArrayList<String>, val context: Context, val source: String) :
    RecyclerView.Adapter<CityAdapter.CityViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder =
        CityViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.city_item_layout,
                parent,
                false
            )
        )

    override fun getItemCount(): Int = categories.size

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        holder.city_name.text = categories[position]


        holder.itemView.setOnClickListener {
            var newFragment: Fragment
            val args = Bundle()
            args.putString("city", categories[position])
            if (source.equals("api") && context.checkInternetConnectivity()) {
                newFragment = CityWeather()
            } else {
                newFragment = Offlinedata()
            }
            newFragment.setArguments(args)
            (context as FragmentActivity).supportFragmentManager.beginTransaction()
                .replace(R.id.fragment, newFragment)
                .commit()

        }
    }

    class CityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val city_name: TextView

        init {
            city_name = itemView.findViewById(R.id.city_name)
        }
    }
}