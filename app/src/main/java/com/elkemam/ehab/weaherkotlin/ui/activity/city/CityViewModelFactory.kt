package com.elkemam.ehab.weaherkotlin.ui.activity.city

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.elkemam.ehab.weaherkotlin.data.repository.WeatherRepo

class CityViewModelFactory(val productRepo : WeatherRepo): ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        return CityViewModel(productRepo) as T
    }

}