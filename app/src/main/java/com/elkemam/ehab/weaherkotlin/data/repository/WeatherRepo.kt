package com.elkemam.ehab.weaherkotlin.data.repository

import android.content.Context
import android.net.ConnectivityManager
//import com.elkemam.ehab.weaherkotlin.data.db.WeatherDataBase
//import com.elkemam.ehab.weaherkotlin.data.entity.WeatherData
import com.elkemam.ehab.weaherkotlin.data.entity.WeatherObject
//import com.example.ayman.neweshteray.data.db.ProductsDb
import com.elkemam.ehab.weaherkotlin.data.network.ApiService
import com.elkemam.ehab.weaherkotlin.data.network.ApiServiceFactory
import com.readystatesoftware.chuck.ChuckInterceptor
import io.reactivex.Observable

class WeatherRepo(val context: Context) {


    companion object {

        // getInstance
        private var instance: WeatherRepo? = null

        fun getInstance(context: Context): WeatherRepo {
            instance = instance ?: WeatherRepo(context)

            return instance as WeatherRepo
        }
    }

    private val service: ApiService by lazy {
        ApiServiceFactory.getInstance(ChuckInterceptor(context))
    }


    fun forecast(q: String, id: String): Observable<WeatherObject> {
        return service.Getforecast(q, id)
    }


}