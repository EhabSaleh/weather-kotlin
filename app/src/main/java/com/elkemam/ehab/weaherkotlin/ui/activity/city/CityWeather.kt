package com.elkemam.ehab.weaherkotlin.ui.activity.city

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.ContentValues
import android.database.Cursor
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.elkemam.ehab.weaherkotlin.R
//import com.elkemam.ehab.weaherkotlin.data.entity.WeatherData
import com.elkemam.ehab.weaherkotlin.data.entity.WeatherObject
import com.elkemam.ehab.weaherkotlin.ui.adapter.DaysAdapter
import com.elkemam.ehab.weaherkotlin.util.DbWorkerThread
//import com.elkemam.ehab.weaherkotlin.data.db.WeatherDataBase
import com.elkemam.ehab.weaherkotlin.data.repository.WeatherRepo
import com.elkemam.ehab.weaherkotlin.databinding.ActivityCityBinding
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.ParseException
import java.text.SimpleDateFormat
import kotlin.collections.ArrayList
import android.database.sqlite.SQLiteDatabase
import android.widget.Toast
import com.elkemam.ehab.weaherkotlin.data.db.DBAdapter
import com.elkemam.ehab.weaherkotlin.util.makeToast
import io.paperdb.Paper


class CityWeather : Fragment() {
    companion object {
        val API_KEY = "b6875c65c9efbf3bfe3f29056564c1f4"
    }

    lateinit var newlist: ArrayList<WeatherObject.X>
    var strings: ArrayList<Int> = ArrayList()
    var toFind: Int = 0
    lateinit var binding: ActivityCityBinding
    var d: DBAdapter? = null
    var db: SQLiteDatabase? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_city, container, false)

        val productRepo = WeatherRepo.getInstance(context!!)
        val Process = activity!!.findViewById<ProgressBar>(R.id.progress)
        Process.visibility = View.VISIBLE

        val viewModel: CityViewModel =
            ViewModelProviders.of(this, CityViewModelFactory(productRepo)).get(CityViewModel::class.java)

        //GET INTENT DATA
        val cityName = arguments!!.getString("city", "")

        // init DB
        d = DBAdapter(context!!)
        db = d!!.getWritableDatabase()

        binding.citymodel = viewModel
        binding.cityName.text = cityName


        var cursor: Cursor? = null
        val sql = "SELECT ID FROM City WHERE city = '$cityName'"
        cursor = db!!.rawQuery(sql, null)

        //UpDate DATA
        if (cursor!!.getCount() > 0) {
            deleteContact(cityName)
        }

        cursor.close()

        // Use RETROFIT with RX JAVA to get Weather Data
        viewModel.Getforecast(cityName, API_KEY).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                newlist = ArrayList<WeatherObject.X>()
                for (i in it.list) {

                    // Convert String To Date
                    val dateString = i.dt_txt
                    val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
                    try {
                        var convertedDate = dateFormat.parse(dateString)
                        toFind = convertedDate.date
                        val r = strings.contains(toFind)
                        if (r) {
                        } else {
                            strings.add(toFind)
                            newlist.add(i)
                        }
                    } catch (e: ParseException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    // Insert/Cache Data to SQLLite
                    insert(
                        cityName,
                        i.dt_txt,
                        i.main.temp.toString(),
                        i.main.humidity.toString(),
                        i.main.pressure.toString(),
                        i.wind.speed.toString(),
                        i.wind.deg.toString()
                    )
                }

                Paper.init(context)
                Paper.book().write("city_name", cityName)
                Paper.book().write("temp", newlist.get(0).main.temp.toString())
                Paper.book().write("press", newlist.get(0).main.pressure.toString())
                Paper.book().write("windSpeed", newlist.get(0).wind.speed.toString())
                Paper.book().write("windDeg", newlist.get(0).wind.deg.toString())
                Paper.book().write("hum", newlist.get(0).main.humidity.toString())

                 Process.visibility = View.GONE
                binding.recycler.adapter = DaysAdapter(newlist, context!!)
                binding.recycler.layoutManager = GridLayoutManager(context!!, 1)
            }
        return binding.root
    }

    fun deleteContact(name: String) {
        d = DBAdapter(context!!)
        db = d!!.getWritableDatabase()
        db!!.delete("City", "city" + " = ?", arrayOf(name))
        db!!.close()
    }

    fun insert(
        city: String,
        dt_txt: String,
        temp: String,
        humidity: String,
        pressure: String,
        speed: String,
        deg: String
    ) {
        d = DBAdapter(context!!)
        db = d!!.getWritableDatabase()
        val cv = ContentValues()

        cv.put("city", city)
        cv.put("dt_txt", dt_txt)
        cv.put("temp", temp)
        cv.put("humidity", humidity)
        cv.put("pressure", pressure)
        cv.put("speed", speed)
        cv.put("deg", deg)

        val Did = db!!.insert("City", null, cv)
        if (Did > -1) {
            context!!.makeToast("data has been inserted")
        } else {
            context!!.makeToast("insertion error")

        }
    }

}
