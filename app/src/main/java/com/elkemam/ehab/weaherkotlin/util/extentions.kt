package com.elkemam.ehab.weaherkotlin.util

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import android.widget.Toast

val TAG: String = "debugMsg"

fun Context.makeToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun Context.checkInternetConnectivity(): Boolean {
    val manager: ConnectivityManager? = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    return manager?.activeNetworkInfo?.isConnected ?: false
}
