package com.elkemam.ehab.weaherkotlin

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.widget.ImageView
import android.widget.RemoteViews
import android.widget.TextView
import io.paperdb.Paper

/**
 * Implementation of App Widget functionality.
 */
class NewAppWidget : AppWidgetProvider() {

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    companion object {

        internal fun updateAppWidget(
            context: Context, appWidgetManager: AppWidgetManager,
            appWidgetId: Int
        ) {


             Paper.init(context)
           val city_name =  Paper.book().read("city_name","")
            val temp = Paper.book().read("temp","")
            val press =  Paper.book().read("press","")
            val windSpeed = Paper.book().read("windSpeed","")
            val windDeg =  Paper.book().read("windDeg","")
            val hum = Paper.book().read("hum","")

             // Construct the RemoteViews object
            val views = RemoteViews(context.packageName, R.layout.new_app_widget)

            views.setTextViewText(R.id.city_name, city_name)
            views.setTextViewText(R.id.temp, temp)
            views.setTextViewText(R.id.hum, hum)
            views.setTextViewText(R.id.press, press)
            views.setTextViewText(R.id.windSpeed, windSpeed)
            views.setTextViewText(R.id.windDeg, windDeg)

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }
}

