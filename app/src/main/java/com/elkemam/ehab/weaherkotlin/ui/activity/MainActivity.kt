package com.elkemam.ehab.weaherkotlin.ui.activity

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ProgressBar
import com.elkemam.ehab.weaherkotlin.R
import com.elkemam.ehab.weaherkotlin.ui.activity.main.Home
import com.elkemam.ehab.weaherkotlin.util.checkInternetConnectivity
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        val Process = findViewById<ProgressBar>(R.id.progress)
        when (item.itemId) {
            R.id.navigation_home -> {

                val args = Bundle()
                args.putString("from", "api")
                val newFragment = Home()
                newFragment.setArguments(args)
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment, newFragment)
                    .commit()
                Process.visibility = View.GONE

                return@OnNavigationItemSelectedListener true

            }
            R.id.aboutus -> {
                supportFragmentManager.beginTransaction().replace(R.id.fragment,
                    About()
                ).addToBackStack(null).commit()
                Process.visibility = View.GONE

                return@OnNavigationItemSelectedListener true
            }
            R.id.offline -> {
                val args = Bundle()
                args.putString("from", "cache")
                val newFragment = Home()
                newFragment.setArguments(args)
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment, newFragment)
                    .commit()
                Process.visibility = View.GONE

                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        // Check if Network Available Or not
        if (baseContext.checkInternetConnectivity()) {

            val args = Bundle()
            args.putString("from", "api")
            val newFragment = Home()
            newFragment.setArguments(args)
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment, newFragment)
                .commit()
        } else {
            val args = Bundle()
            args.putString("from", "cache")
            val newFragment = Home()
            newFragment.setArguments(args)
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment, newFragment)
                .commit()

        }


        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun onBackPressed() {

    }
}
