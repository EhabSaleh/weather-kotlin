package com.elkemam.ehab.weaherkotlin.ui.fragment.offline

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.ContentValues
import android.database.Cursor
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.elkemam.ehab.weaherkotlin.R
//import com.elkemam.ehab.weaherkotlin.data.entity.WeatherData
import com.elkemam.ehab.weaherkotlin.data.entity.WeatherObject
import com.elkemam.ehab.weaherkotlin.ui.adapter.DaysAdapter
import com.elkemam.ehab.weaherkotlin.util.DbWorkerThread
//import com.elkemam.ehab.weaherkotlin.data.db.WeatherDataBase
import com.elkemam.ehab.weaherkotlin.data.repository.WeatherRepo
import com.elkemam.ehab.weaherkotlin.databinding.ActivityCityBinding
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.ParseException
import java.text.SimpleDateFormat
import kotlin.collections.ArrayList
import android.database.sqlite.SQLiteDatabase
import android.widget.TextView
import android.widget.Toast
import com.elkemam.ehab.weaherkotlin.data.db.DBAdapter
import com.elkemam.ehab.weaherkotlin.data.entity.CacheModel
import com.elkemam.ehab.weaherkotlin.ui.adapter.CacheAdapter
import com.elkemam.ehab.weaherkotlin.util.makeToast


class Offlinedata : Fragment() {

    var CacheModel = CacheModel()
    lateinit var newlist: ArrayList<CacheModel>
    var toFind: Int = 0
    var strings: ArrayList<Int> = ArrayList()
    lateinit var binding: ActivityCityBinding
    @RequiresApi(Build.VERSION_CODES.N)
    var d: DBAdapter? = null
    var db: SQLiteDatabase? = null
    var i = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.activity_city, container, false)
        val cityName = arguments!!.getString("city", "")
        d = DBAdapter(context!!)
        db = d!!.getWritableDatabase()

        // get List of weather from DB by city name
        getDataFromDatabase(cityName)

        return binding.root
    }


    fun retrieveRecords(city_name: String): Cursor? {
        var c: Cursor? = null
        c = db!!.rawQuery("select * from City where city='$city_name'", null)
        return c
    }


    fun getDataFromDatabase(city_name: String) {
        val Process = activity!!.findViewById<ProgressBar>(R.id.progress)
        Process.visibility = View.VISIBLE

        try {
            var cursor: Cursor?
            var mylist: ArrayList<CacheModel> = ArrayList()

            d = DBAdapter(context!!)
            db = d!!.getWritableDatabase()

            cursor = retrieveRecords(city_name)
            if (cursor!!.count != 0) {
                if (cursor.moveToFirst()) {


                    do {
                        // insert value in Object and add to my list
                        val city = cursor.getString(1)
                        val dt_txt = cursor.getString(2)
                        val temp = cursor.getString(3)
                        val humidity = cursor.getString(4)
                        val pressure = cursor.getString(5)
                        val speed = cursor.getString(6)
                        val deg = cursor.getString(7)
                        CacheModel = CacheModel()
                        CacheModel.city = city
                        CacheModel.dt_txt = dt_txt
                        CacheModel.temp = temp
                        CacheModel.humidity = humidity
                        CacheModel.pressure = pressure
                        CacheModel.speed = speed
                        CacheModel.deg = deg
                        mylist.add(i, CacheModel)
                        i++
                    } while (cursor.moveToNext())
                }
                db!!.close()


                // sort data and get only days
                newlist = ArrayList<CacheModel>()
                for (i in mylist) {
                    val dateString = i.dt_txt
                    val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
                    try {
                        var convertedDate = dateFormat.parse(dateString)
                        toFind = convertedDate.date
                        val r = strings.contains(toFind)
                        if (r) {
                        } else {
                            strings.add(toFind)
                            newlist.add(i)
                        }
                    } catch (e: ParseException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                }

                Process.visibility = View.GONE
                binding.recycler.adapter = CacheAdapter(newlist, context!!)
                binding.recycler.layoutManager = GridLayoutManager(context!!, 1)
            } else {
                val no_data: TextView = binding.noData
                no_data.visibility = View.VISIBLE
                Process.visibility = View.GONE
            }
            cursor.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}
