package com.elkemam.ehab.weaherkotlin.data.entity

data class CacheModel(
    var city: String,
    var dt_txt: String,
    var temp: String,
    var humidity: String,
    var pressure: String,
    var speed: String,
    var deg: String
){
    constructor():this("","","","","","",
        "")
}