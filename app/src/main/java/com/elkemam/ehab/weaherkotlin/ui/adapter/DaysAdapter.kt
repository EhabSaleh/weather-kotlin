package com.elkemam.ehab.weaherkotlin.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.elkemam.ehab.weaherkotlin.R
import com.elkemam.ehab.weaherkotlin.data.entity.WeatherObject
import java.text.ParseException
import java.text.SimpleDateFormat

class DaysAdapter(val categories: List<WeatherObject.X>, val context: Context) :
    RecyclerView.Adapter<DaysAdapter.CityViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder =
        CityViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.day_item_layout,
                parent,
                false
            )
        )

    override fun getItemCount(): Int = categories.size

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {


        val dateString = categories[position].dt_txt
        try {

            // get  Day/Month/Year and remove time
            val str = dateString.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val theday = str[2]
            val month = str[1]
            val year = str[0]
            val indexOfLast = theday.lastIndexOf(" ")
            var day = theday
            if (indexOfLast >= 0) day = theday.substring(0, indexOfLast)

            holder.city_name.text = "$day/$month/$year "


            holder.temp.setText(categories[position].main.temp.toString())
            holder.hum.setText(categories[position].main.humidity.toString())
            holder.press.setText(categories[position].main.pressure.toString())
            holder.windSpeed.setText(categories[position].wind.speed.toString())
            holder.windDeg.setText(categories[position].wind.deg.toString())


        } catch (e: ParseException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }


    }

    class CityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val city_name: TextView
        var temp: TextView
        var press: TextView
        var windSpeed: TextView
        var windDeg: TextView
        var hum: TextView
        var imgView: ImageView

        init {
            city_name = itemView.findViewById(R.id.daydate)

            temp = itemView.findViewById(R.id.temp)
            hum = itemView.findViewById(R.id.hum)
            press = itemView.findViewById(R.id.press)
            windSpeed = itemView.findViewById(R.id.windSpeed)
            windDeg = itemView.findViewById(R.id.windDeg)
            imgView = itemView.findViewById(R.id.condIcon)

        }
    }
}